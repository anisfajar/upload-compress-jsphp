<?php

//controller
class Videoplayer extends CI_Controller
{
    public function do_upload()
    {
        $namafilevideo = 'video'.date("YmdHis").'.mp4';
        $config3['upload_path']   = 'public/video/';
        $config3['file_name']     = $namafilevideo;
        $config3['allowed_types'] = "mp4|3gp";
        $config3['remove_spaces'] = true;
        $config3['overwrite']  = FALSE;

        $this->load->library('upload');
        $this->upload->initialize($config3);

        $this->upload->do_upload('file');
        $dataimg = $this->upload->data('file_name');

        $namavideo = $this->input->post('namavideo');
        $datasimpan = [
            'namavideo' => $namavideo,
            'video' => "v".$namafilevideo,
            'datetimecreate' => date("Y-m-d H:i:s"),
            'is_active' => '0',
        ];

        $simpan = $this->m_videoplayer->savevideo($datasimpan);

        if ($simpan == TRUE) {
            $output=null;
            exec("C:\\ffmpeg\\bin\\ffmpeg.exe -i ".FCPATH."public/video/".$namafilevideo." -b 600k ".FCPATH."public/video/"."v".$namafilevideo,$output);
            echo "Berhasil simpan data";
            unlink(FCPATH."public/video/".$namafilevideo);
        } else {
            echo $this->upload->display_errors();
        }
    }
}


//view

<link rel="stylesheet" href="<?=base_url('public/assetsa/')?>assets/vendor/sweetalert2/sweetalert2.css">
<script src="<?=base_url('public/assetsa/')?>assets/vendor/sweetalert2/sweetalert2.js"></script>
<div class="content-heading clearfix">
    <div class="heading-left">
        <h1 class="page-title">Tambah Video Player</h1>
        <p class="page-subtitle">Silahkan isi sesuai label isian yang telah tertera.</p>
    </div>
    <ul class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Main</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active">Video player</li>
    </ul>
</div>
<div class="col-md-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Silahkan isi di bawah ini</h3>
        </div>
        <div class="panel-body">
            <form enctype="multipart/form-data" id="submit">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                <div class="form-group">
                    <label>Nama Video</label>
                    <input type="text" autocomplete="off" name="namavideo" class="form-control" id="namavideo" placeholder="Nama Video">
                </div>
                <div class="form-group">
                    <label>Video</label>
                    <input type="file" id="dropifyfile" name="file">
                </div>
    
                <button type="submit" id="sub" class="btn btn-primary"><i class="fa fa-save"></i>
                    <span>Simpan</span>
                </button>

                <a href="<?= site_url('videoplayer') ?>"><button class="btn btn-success" type="button"><i class="fa fa-refresh"></i>Kembali</button></a>
            </form>
            <br>
        
            <div class="w-50 mx-auto" id="progress" style="display: none;">
            <div class="progress">
                 <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">Proses Uploading (0%)</div></div>
              <hr>  
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {
        $('#thumbnail').dropify()
        $('#submit').submit(function(e){
            var progress = $('.progress-bar');
            var progressCon = $('#progress');
            e.preventDefault(); 
            $.ajax({
                url:'<?= site_url();?>admin/videoplayer/do_upload',
                type:"post",
                data:new FormData(this),
                contentType:false,
                processData:false,
                beforeSend: () => {
                    $(progressCon).slideDown();
                },
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = (evt.loaded / evt.total) * 100;
                            $(progress).attr("aria-valuenow", percentComplete.toFixed(0));
                            $(progress).width(percentComplete.toFixed(0)+'%');
                            $(progress).text('Proses uploading ('+percentComplete.toFixed(2)+'%). Saat ini sedang proses compress video tunggu sampai selesai dan muncul notifikasi');
                        }
                    },false);
                    return xhr;
                },
                success: function(data){
                    $(progress).text('Uploaded Berhasil (100%)');
                    $(progress).addClass('bg-success');

                    swal({
                        title: "Sukses",
                        text: data,
                        type: "success"
                    }).then(function() {
                        window.location = "<?=site_url('videoplayer')?>";
                    })
            }
            });
        }); 

        var drEvent = $('#dropifyfile').dropify();
        drEvent.on('dropify.beforeClear', function(event, element)
        {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function(event, element)
        {
            alert('File deleted');
        });
    })
</script>
